'use strict';

const gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	gutil = require('gulp-util'),
	cssImport = require('gulp-cssimport'),
	argv = require('yargs').argv;


const srcPath = './wp-content/themes/ketoan/assets';

var config = {
	sass: srcPath + '/sass/**/*.scss',
	fileStyle: srcPath + '/sass/style.scss',
	css: srcPath + '/css'
};

var onError = function (err) {
	console.log('An error occurred:', gutil.colors.cyan.bold(err.message));
	gutil.beep();
	this.emit('end');
};

function css() {
	let gulpCss = gulp.src(config.fileStyle)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: argv.mode === 'production' ? 'compressed' : 'expanded',
			quiet: true			
		}).on('error', sass.logError))
		.pipe(cssImport())
		.pipe(autoprefixer('safari >= 8', 'ios >= 10', 'android >= 8', 'ie >=11', 'last 3 version'));

	if(!argv.mode || argv.mode === 'develop'){
		gulpCss.pipe(sourcemaps.write())		
	}
	gulpCss.pipe(gulp.dest(config.css));
	return gulpCss;
}

function watchFiles() {
	gulp.watch(config.sass, css);	
}



exports.css = css;
exports.build = watchFiles;
