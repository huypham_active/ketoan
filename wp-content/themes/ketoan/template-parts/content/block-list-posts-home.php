<?php
    $argListPosts = array(
        'numberposts'      => 4,
        'category'         => '1,3',
        'post_type'        => 'post'
    );
    $listpost = get_posts($argListPosts);
    if ( $listpost ) {
        foreach ( $listpost as $post ) :
        ?>
            <div><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'single-post-thumbnail' ); ?>" /></div>
            <h2><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>
            <?php echo $post->post_excerpt; ?>
            <span><?php echo get_the_date('d/m/Y') ?></span>
            <?php
                $catOfPosts = get_the_category($post->ID);
                $length = count($catOfPosts);
                echo '<p class="cats">';
                foreach($catOfPosts as $cat){
                   
                    echo '<a href="'.get_category_link($cat->term_id).'">'.$cat->name.'</a>';
                }
                echo '</p>';
            ?>
        <?php
        endforeach; 
    }
?>
