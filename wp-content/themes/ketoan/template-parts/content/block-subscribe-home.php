<div class="block-subscribe-home">
    <div class="container">
        <div class="content">
            <h3>Đăng ký nhận thông tin về Kế Toán Thuế</h3>
            <form class="form-inline">
                <label>Họ tên:&nbsp;&nbsp;</label>
                <input type="text" id="request_assistant_name" placeholder="Trần Văn A"  class="form-control">&nbsp;
                <label>Email:&nbsp;&nbsp;</label>
                <input type="email" id="request_assistant_email" placeholder="abc@example.com" style="width:200px" class="form-control">&nbsp;
                <label>Số điện thoại:&nbsp;&nbsp;</label>
                <input type="text" id="request_assistant_phone" placeholder="0123.456.789" style="width: 200px" class="form-control">&nbsp;
                <button type="button" id="send_request_assistant" class="btn btn-default">Gửi yêu cầu <i class="fa fa-long-arrow-right"></i></button>
            </form>
        </div>
    </div>
</div>