<?php

$infoField = get_fields(23);
if( $infoField ): 
?>
    <div class="header-top">
        <div class="container">
            <div class="header-info">
                <a href="tel:<?php echo $infoField['phone']?>" class="button"><?php echo $infoField['phone']?></a>
                <a href="mailto:<?php echo $infoField['email']?>" class="button"><?php echo $infoField['email']?></a>
            </div>
            <div class="header-search">
                <form role="search"  method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">                
                    <input type="text" placeholder="Nhập từ khóa tìm kiếm" id="header-search-form" class="search-field" value="<?php echo get_search_query(); ?>" name="s" />
                    <button type="submit" class="search-submit">Search</button>
                </form>
            </div>
        </div>
    </div>

<?php endif; ?>


