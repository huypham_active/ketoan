<?php
/**
 * Displays the site header.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */


?>

<header id="masthead" role="banner">
	<?php get_template_part( 'template-parts/header/site-header-top' ); ?>
	<div class="header-main">
		<div class="container">
			<?php get_template_part( 'template-parts/header/site-logo' ); ?>
			<?php get_template_part( 'template-parts/header/site-nav' ); ?>
		</div>
	</div>
	
	

</header><!-- #masthead -->
