<?php
/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<div class="header-menu">
	<?php
		wp_nav_menu(
			array(
				'menu' => 'main-menu',
				'theme_location'  => 'primary',
				'menu_class'      => 'menu-wrapper',
				'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
				'fallback_cb'     => false,
			)
		);
	?>
</div>

